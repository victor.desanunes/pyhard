import unittest
import numpy as np
import pandas as pd
from sklearn.datasets import load_boston, load_diabetes

from pyhard.regression import Regressor, _reg_dict


class TestRegression(unittest.TestCase):
    def test_full(self):
        boston = load_boston()
        data = pd.DataFrame(data=np.c_[boston.data, boston.target],
                            columns=boston.feature_names.tolist() + ['target'])
        regressor = Regressor(data)
        result = regressor.run_all()

        self.assertEqual(len(result.columns), len(_reg_dict.keys()))
        self.assertFalse(result.isna().any().any())
        self.assertEqual(len(data), len(result))

    def test_score(self):
        y_true = np.random.uniform(size=1000)
        y_pred = np.random.uniform(size=1000) + 10 * np.array(range(len(y_true)))
        assert y_true.shape == y_pred.shape

        s, p = Regressor.score('absolute_error', y_true, y_pred)
        self.assertTrue(np.all(np.diff(s) >= 0))
        self.assertTrue(np.all(np.diff(p) <= 0))
        self.assertTrue(np.all(np.logical_and(p >= 0, p <= 1)))
